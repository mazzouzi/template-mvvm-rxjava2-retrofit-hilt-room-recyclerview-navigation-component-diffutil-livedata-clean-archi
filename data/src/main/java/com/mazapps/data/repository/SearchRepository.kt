package com.mazapps.data.repository

import com.mazapps.data.api.INetworkDataSource
import com.mazapps.data.db.IDbDataSource
import com.mazapps.data.mapper.toData
import com.mazapps.data.mapper.toDomain
import com.mazapps.domain.model.Search
import com.mazapps.domain.repository.ISearchRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author morad.azzouzi on 01/12/2019.
 */
class SearchRepository @Inject constructor(
    private val networkDataSource: INetworkDataSource,
    private val dbDataSource: IDbDataSource
) : ISearchRepository {

    override fun fetchSearchInfoFromApi(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>> =
        networkDataSource.fetchSearchInfo(action, format, list, keyword).map { it.toDomain() }

    override fun fetchSearchInfoFromDb(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>> =
        dbDataSource.fetchRelatedSearch().toObservable().map { it.toDomain() }

    override fun insert(search: Search) {
        dbDataSource.insert(search.toData())
    }
}