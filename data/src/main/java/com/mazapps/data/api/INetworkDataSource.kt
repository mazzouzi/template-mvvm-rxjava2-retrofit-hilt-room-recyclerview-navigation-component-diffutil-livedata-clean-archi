package com.mazapps.data.api

import com.mazapps.data.model.SearchData
import io.reactivex.Observable

/**
 * @author morad.azzouzi on 01/12/2019.
 */
interface INetworkDataSource {

    fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<SearchData>>
}