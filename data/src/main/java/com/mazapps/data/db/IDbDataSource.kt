package com.mazapps.data.db

import com.mazapps.data.model.SearchData
import io.reactivex.Single

/**
 * @author morad.azzouzi on 13/11/2020.
 */
interface IDbDataSource {

    fun fetchRelatedSearch(): Single<List<SearchData>>

    fun insert(search: SearchData)
}