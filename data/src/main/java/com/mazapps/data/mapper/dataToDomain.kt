package com.mazapps.data.mapper

import com.mazapps.data.model.SearchData
import com.mazapps.domain.model.Search

/**
 * @author morad.azzouzi on 25/12/2020.
 */
internal fun List<SearchData>.toDomain(): List<Search> =
    this.map {
        Search(
            it.ns,
            it.title,
            it.pageid,
            it.size,
            it.snippet,
            it.timestamp,
            it.wordcount
        )
    }