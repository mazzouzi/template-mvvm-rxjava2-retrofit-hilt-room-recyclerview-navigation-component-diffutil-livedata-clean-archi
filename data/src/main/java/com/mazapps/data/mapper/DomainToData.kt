package com.mazapps.data.mapper

import com.mazapps.data.model.SearchData
import com.mazapps.domain.model.Search

/**
 * @author morad.azzouzi on 25/12/2020.
 */
internal fun Search.toData(): SearchData =
    SearchData(
        this.ns,
        this.title,
        this.pageid,
        this.size,
        this.snippet,
        this.timestamp,
        this.wordcount
    )