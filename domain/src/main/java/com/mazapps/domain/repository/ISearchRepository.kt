package com.mazapps.domain.repository

import com.mazapps.domain.model.Search
import io.reactivex.Observable
import io.reactivex.Single

/**
 * @author morad.azzouzi on 11/11/2020.
 */
interface ISearchRepository {

    fun fetchSearchInfoFromApi(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>>

    fun fetchSearchInfoFromDb(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>>

    fun insert(search: Search)
}