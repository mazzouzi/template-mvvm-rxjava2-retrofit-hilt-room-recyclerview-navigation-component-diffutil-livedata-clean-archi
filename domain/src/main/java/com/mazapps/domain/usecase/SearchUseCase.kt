package com.mazapps.domain.usecase

import com.mazapps.domain.model.Search
import com.mazapps.domain.repository.ISearchRepository
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author morad.azzouzi on 23/12/2020.
 */
class SearchUseCase @Inject constructor(private val searchRepository: ISearchRepository) {

    operator fun invoke(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>> =
        Observable
            .concatArrayEagerDelayError(
                searchRepository.fetchSearchInfoFromDb(action, format, list, keyword),
                searchRepository
                    .fetchSearchInfoFromApi(action, format, list, keyword)
                    .doAfterNext { data ->
                        data.forEach { searchRepository.insert(it) }
                    }
            )
            .subscribeOn(Schedulers.io())
}