package com.mazapps.tabesto.ui.main.viewmodel

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.FileInputStream
import java.lang.reflect.Type
import java.net.URL

/**
 * @author morad.azzouzi on 06/12/2019.
 */
/**
 * This extension file allows to use a filename of json as input to Gson.
 * This file will be used by unit tests
 */
fun <T> Gson.fromJson(fileName: String, classLoader: ClassLoader, type: Type): T {
    val resource: URL = classLoader.getResource(fileName)
    val inputStream = FileInputStream(resource.path)
    inputStream.use {
        return fromJson(JsonReader(it.bufferedReader()), type)
    }
}

/**
 * This extension file allows to use a filename of json as input to Gson.
 * This file will be used by unit tests
 */
fun <T> Gson.fromJson(fileName: String, classLoader: ClassLoader, clazz: Class<T>): T {
    val resource: URL = classLoader.getResource(fileName)
    val inputStream = FileInputStream(resource.path)
    inputStream.use {
        return fromJson(JsonReader(it.bufferedReader()), clazz)
    }
}