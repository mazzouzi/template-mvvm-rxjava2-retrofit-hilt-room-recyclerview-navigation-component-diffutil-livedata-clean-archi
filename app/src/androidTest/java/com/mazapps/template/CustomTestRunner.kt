package com.mazapps.template

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import dagger.hilt.android.testing.HiltTestApplication

/**
 * A custom runner to set up the instrumented application class for tests.
 *
 * @author morad.azzouzi on 14/11/2020.
 */

class CustomTestRunner : AndroidJUnitRunner() {

    override fun newApplication(cl: ClassLoader?, name: String?, context: Context?): Application? {
        return super.newApplication(cl, HiltTestApplication::class.java.name, context)
    }
}