package com.mazapps.template.di.module

import com.mazapps.data.api.INetworkDataSource
import com.mazapps.data.db.IDbDataSource
import com.mazapps.template.framework.source.DbDataSource
import com.mazapps.template.framework.source.NetworkDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 12/11/2020.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class ApiModule {

    /**
     * The @Binds annotation tells Hilt which implementation to use when it needs to provide
     * an instance of an interface.
     * Parameters are implementations and return types are the interfaces provided
     */
    @Binds
    @Singleton
    abstract fun bindApiHelper(appApiHelper: NetworkDataSource): INetworkDataSource

    @Binds
    @Singleton
    abstract fun bindDbHelper(appDbHelper: DbDataSource): IDbDataSource
}