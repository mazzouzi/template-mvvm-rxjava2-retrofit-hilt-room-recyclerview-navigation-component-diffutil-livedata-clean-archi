package com.mazapps.template.di.module

import com.mazapps.data.repository.SearchRepository
import com.mazapps.domain.repository.ISearchRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

/**
 * @author morad.azzouzi on 20/12/2020.
 */
@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class RepositoryModule {

    @Binds
    @ActivityRetainedScoped
    abstract fun bindSearchRepository(repositorySearch: SearchRepository): ISearchRepository
}