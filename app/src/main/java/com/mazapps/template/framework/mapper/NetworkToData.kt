package com.mazapps.template.framework.mapper

import com.mazapps.data.model.SearchData
import com.mazapps.template.model.dto.Wikipedia

/**
 * @author morad.azzouzi on 25/12/2020.
 */
internal fun Wikipedia?.toData(): List<SearchData>? =
    this?.query?.search?.map {
        SearchData(
            it.ns ?: 0,
            it.title.orEmpty(),
            it.pageid ?: 0,
            it.size ?: 0,
            it.snippet.orEmpty(),
            it.timestamp.orEmpty(),
            it.wordcount ?: 0
        )
    }