package com.mazapps.template.framework.source

import com.mazapps.data.api.INetworkDataSource
import com.mazapps.data.model.SearchData
import com.mazapps.template.framework.api.RetrofitApiService
import com.mazapps.template.framework.mapper.toData
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author morad.azzouzi on 01/12/2019.
 */
open class NetworkDataSource @Inject constructor(
    private val retrofit: RetrofitApiService
) : INetworkDataSource {

    override fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<SearchData>> =
        retrofit.fetchSearchInfo(action, format, list, keyword).map {
            it.body()?.toData()
        }
}