package com.mazapps.template.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mazapps.template.model.entity.SearchEntity
import io.reactivex.Single

/**
 * @author morad.azzouzi on 13/11/2020.
 */
@Dao
interface DaoSearch {

    @Query("SELECT * FROM related_search")
    fun fetchRelatedSearch(): Single<List<SearchEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(search: SearchEntity)
}