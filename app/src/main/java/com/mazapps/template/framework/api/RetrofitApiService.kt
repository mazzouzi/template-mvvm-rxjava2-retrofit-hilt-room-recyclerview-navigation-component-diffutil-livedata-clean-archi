package com.mazapps.template.framework.api

import com.mazapps.data.api.ApiEndPoint
import com.mazapps.template.model.dto.Wikipedia
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author morad.azzouzi on 11/11/2020.
 */
interface RetrofitApiService {

    @GET(ApiEndPoint.SEARCH_COUNT)
    fun fetchSearchInfo(
        @Query("action") action: String,
        @Query("format") format: String,
        @Query("list") list: String,
        @Query("srsearch") srsearch: String
    ): Observable<Response<Wikipedia>>
}