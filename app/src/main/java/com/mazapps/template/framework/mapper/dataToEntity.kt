package com.mazapps.template.framework.mapper

import com.mazapps.data.model.SearchData
import com.mazapps.template.model.entity.SearchEntity

/**
 * @author morad.azzouzi on 25/12/2020.
 */
internal fun SearchData.toEntity(): SearchEntity =
    SearchEntity(
        this.ns,
        this.title,
        this.pageid,
        this.size,
        this.snippet,
        this.timestamp,
        this.wordcount
    )