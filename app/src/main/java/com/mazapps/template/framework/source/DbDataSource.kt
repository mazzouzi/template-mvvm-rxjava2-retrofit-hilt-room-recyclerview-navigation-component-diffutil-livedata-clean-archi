package com.mazapps.template.framework.source

import com.mazapps.data.db.IDbDataSource
import com.mazapps.data.model.SearchData
import com.mazapps.template.framework.db.DaoSearch
import com.mazapps.template.framework.mapper.toData
import com.mazapps.template.framework.mapper.toEntity
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author morad.azzouzi on 13/11/2020.
 */
class DbDataSource @Inject constructor(
    private val dao: DaoSearch
) : IDbDataSource {

    override fun fetchRelatedSearch(): Single<List<SearchData>> =
        dao.fetchRelatedSearch().map {
            if (it.isEmpty())
                null
            else
                it.toData()
        }

    override fun insert(search: SearchData) {
        dao.insert(search.toEntity())
    }
}