package com.mazapps.template.state

import com.mazapps.template.state.StatusEnum.*

/**
 * @author morad.azzouzi on 13/12/2020.
 */
data class Resource<out T>(val status: StatusEnum, val data: T?, val message: String?) {

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null)
        }

        fun <T> error(msg: String? = null): Resource<T> {
            return Resource(ERROR, null, msg)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(LOADING, data, null)
        }
    }
}

enum class StatusEnum {
    LOADING,
    SUCCESS,
    ERROR
}

