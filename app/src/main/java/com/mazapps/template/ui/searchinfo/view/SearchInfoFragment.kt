package com.mazapps.template.ui.searchinfo.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ViewFlipper
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.mazapps.template.databinding.FragmentSearchInfoBinding
import com.mazapps.template.helper.RxBusHelper
import com.mazapps.template.helper.ViewBindingHelper
import com.mazapps.template.state.StatusEnum
import com.mazapps.template.ui.interfaces.LoadingFlipperInterface
import com.mazapps.template.ui.interfaces.ViewBindingInterface
import com.mazapps.template.ui.searchinfo.event.SearchInfoClickEvent
import com.mazapps.template.ui.searchinfo.model.SearchPresentation
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoEnum
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@AndroidEntryPoint
class SearchInfoFragment :
    Fragment(),
    LoadingFlipperInterface,
    ViewBindingInterface<FragmentSearchInfoBinding> by ViewBindingHelper() {

    @Inject lateinit var viewModel: SearchInfoViewModel
    @Inject lateinit var disposables: CompositeDisposable
    @Inject lateinit var adapter: SearchInfoAdapter
    @Inject lateinit var itemDecoration: DividerItemDecoration

    override val viewFlipper: ViewFlipper?
        get() = binding?.searchViewFlipper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initBinding(
        FragmentSearchInfoBinding.inflate(inflater),
        viewLifecycleOwner,
        SearchInfoFragment::class.java.name
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerEvent()
        observeLiveData()
        initView()
        fetchData()
    }

    private fun observerEvent() {
        disposables.add(
            RxBusHelper
                .events
                .subscribe {
                    when (it) {
                        is SearchInfoClickEvent -> onSearchInfoItemClick(it)
                    }
                }
        )
    }

    private fun onSearchInfoItemClick(event: SearchInfoClickEvent) {
        val itemData = viewModel.getItemDataAt(event.position) as SearchPresentation
        val action = SearchInfoFragmentDirections.actionSearchInfoToSearchDetails(itemData)
        findNavController().navigate(action)
    }

    private fun observeLiveData() {
        viewModel.items.observe(viewLifecycleOwner) {
            when (it.status) {
                StatusEnum.LOADING -> {
                    showLoaderView()
                }
                StatusEnum.SUCCESS -> {
                    onFetchSearchInfoSuccess(it.data)
                }
                StatusEnum.ERROR -> {
                    onFetchSearchInfoError()
                }
            }
        }
    }

    private fun onFetchSearchInfoSuccess(data: MutableList<Pair<SearchInfoEnum, Any>>?) {
        data?.let {
            adapter.dispatchUpdates(data)
            showContent()
        }
    }

    private fun onFetchSearchInfoError() {
        showNoResultView()
        adapter.notifyDataSetChanged()
    }

    private fun initView() {
        requireBinding {
            recyclerView.adapter = adapter
            recyclerView.setHasFixedSize(true)
            recyclerView.addItemDecoration(itemDecoration)
        }
    }

    private fun fetchData() {
        if (viewModel.isEmpty) {
            disposables.add(viewModel.fetchSearchInfo())
        }  else {
            showContent()
            adapter.notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }
}