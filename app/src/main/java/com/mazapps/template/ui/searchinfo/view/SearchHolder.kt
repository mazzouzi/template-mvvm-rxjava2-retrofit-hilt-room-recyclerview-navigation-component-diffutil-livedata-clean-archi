package com.mazapps.template.ui.searchinfo.view

import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.databinding.ItemSearchInfoBinding
import com.mazapps.template.helper.RxBusHelper
import com.mazapps.template.ui.searchinfo.event.SearchInfoClickEvent
import com.mazapps.template.ui.searchinfo.model.SearchPresentation

/**
 * @author morad.azzouzi on 22/11/2020.
 */
class SearchHolder(
    private val binding: ItemSearchInfoBinding
) : RecyclerView.ViewHolder(binding.root) {

    init {
        itemView.setOnClickListener {
            RxBusHelper.setEvent(SearchInfoClickEvent(adapterPosition))
        }
    }

    fun bind(search: SearchPresentation) {
        binding.title.text = search.title
    }
}