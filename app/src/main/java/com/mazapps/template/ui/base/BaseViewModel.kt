package com.mazapps.template.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mazapps.template.state.Resource

/**
 * @author morad.azzouzi on 26/12/2020.
 */
open class BaseViewModel<T> : ViewModel() {

    protected val _items = MutableLiveData<Resource<T>>()

    protected val data: T?
        get() = items.value?.data

    val items: LiveData<Resource<T>>
        get() = _items
}