package com.mazapps.template.ui.searchinfo.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.databinding.ItemSearchInfoBinding
import com.mazapps.template.databinding.ItemSearchInfoHeaderBinding

import com.mazapps.template.ui.searchinfo.diffutil.SearchInfoDiffCallback
import com.mazapps.template.ui.searchinfo.model.SearchPresentation
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoEnum
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel

class SearchInfoAdapter(
    private val viewModel: SearchInfoViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var currentList: MutableList<Pair<SearchInfoEnum, Any>> = mutableListOf()

    fun dispatchUpdates(data: MutableList<Pair<SearchInfoEnum, Any>>) {
        val diff = DiffUtil.calculateDiff(SearchInfoDiffCallback(currentList, data), true)
        currentList.clear()
        currentList.addAll(data)
        diff.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            SearchInfoEnum.SEARCH.ordinal -> SearchHolder(
                ItemSearchInfoBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> SearchHeaderHolder(
                ItemSearchInfoHeaderBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SearchHolder -> holder.bind(viewModel.getItemDataAt(position) as SearchPresentation)
            is SearchHeaderHolder -> holder.bind(viewModel.getItemDataAt(position) as String)
        }
    }

    override fun getItemCount(): Int = viewModel.itemCount

    override fun getItemViewType(position: Int): Int = viewModel.getItemViewTypeAt(position)
}
