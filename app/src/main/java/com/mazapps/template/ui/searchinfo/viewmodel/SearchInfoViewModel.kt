package com.mazapps.template.ui.searchinfo.viewmodel

import com.mazapps.domain.model.Search
import com.mazapps.domain.usecase.SearchUseCase
import com.mazapps.template.state.Resource
import com.mazapps.template.ui.base.BaseViewModel
import com.mazapps.template.ui.searchinfo.mapper.toPresentation
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * @author morad.azzouzi on 11/11/2020.
 */
const val KEYWORD = "sasuke"
class SearchInfoViewModel(
    private val searchUseCase: SearchUseCase
) : BaseViewModel<MutableList<Pair<SearchInfoEnum, Any>>>() {

    init {
        _items.value = Resource.loading()
    }

    val itemCount: Int
        get() = data?.size ?: 0
    val isEmpty: Boolean
        get() = itemCount == 0

    fun getItemViewTypeAt(position: Int): Int = data?.get(position)?.first?.ordinal ?: 0
    fun getItemDataAt(position: Int): Any? = data?.get(position)?.second

    fun fetchSearchInfo(): Disposable =
        searchUseCase("query", "json", "search", KEYWORD)
            .observeOn(AndroidSchedulers.mainThread(), true)
            .subscribe(::onSuccess, ::onError)

    private fun onSuccess(search: List<Search>) {
        mutableListOf<Pair<SearchInfoEnum, Any>>().apply {
            add(Pair(SearchInfoEnum.HEADER, KEYWORD))
            addAll(search.map { Pair(SearchInfoEnum.SEARCH, it.toPresentation()) })
            _items.value = Resource.success(this)
        }
    }

    private fun onError(e: Throwable) {
        if (isEmpty) {
            _items.value = Resource.error(e.message)
        }
    }
}

enum class SearchInfoEnum {
    HEADER,
    SEARCH
}