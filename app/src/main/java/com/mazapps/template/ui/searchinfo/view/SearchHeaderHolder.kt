package com.mazapps.template.ui.searchinfo.view

import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.R
import com.mazapps.template.databinding.ItemSearchInfoHeaderBinding

/**
 * @author morad.azzouzi on 22/11/2020.
 */
class SearchHeaderHolder(
    private val binding: ItemSearchInfoHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(value: String) {
        binding.keyword.text = itemView.resources.getString(R.string.keyword, value)
    }
}