package com.mazapps.template.ui.searchinfo.mapper

import com.mazapps.domain.model.Search
import com.mazapps.template.ui.searchinfo.model.SearchPresentation

/**
 * @author morad.azzouzi on 25/12/2020.
 */
internal fun Search.toPresentation(): SearchPresentation =
    SearchPresentation(
        this.ns,
        this.title,
        this.pageid,
        this.size,
        this.snippet,
        this.timestamp,
        this.wordcount
    )
