package com.mazapps.template.ui.interfaces

import android.widget.ViewFlipper

/**
 * This interface defines the behavior of a fragment which has to switch the display between a
 * recycler view, a loader, a no result view, and a not logged in view.
 *
 * A view flipper containing all these view should be defined in the layout.
 *
 * /!\ The order of the views should be the following one: /!\
 * (0) Loader
 * (1) Content
 * (2) No result view
 * (3) Not logged in view
 *
 * @author morad.azzouzi on 22/11/2020.
 */
interface LoadingFlipperInterface {

    val viewFlipper: ViewFlipper?

    fun showLoaderView() {
        viewFlipper?.displayedChild = 0
    }

    fun showContent() {
        viewFlipper?.displayedChild = 1
    }

    fun showNoResultView() {
        viewFlipper?.displayedChild = 2
    }

    fun showNotLoggedInView() {
        viewFlipper?.displayedChild = 3
    }
}