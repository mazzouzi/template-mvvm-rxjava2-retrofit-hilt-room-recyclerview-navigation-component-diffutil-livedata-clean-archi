package com.mazapps.template.model.dto

data class Wikipedia(
    val batchcomplete: String?,
    val `continue`: Continue?,
    val query: Query?
)