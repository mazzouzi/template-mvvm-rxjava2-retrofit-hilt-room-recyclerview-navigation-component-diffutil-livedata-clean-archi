package com.mazapps.template.model.dto

data class Search(
    val ns: Int?,
    val title: String?,
    val pageid: Int?,
    val size: Int?,
    val snippet: String?,
    val timestamp: String?,
    val wordcount: Int?
)