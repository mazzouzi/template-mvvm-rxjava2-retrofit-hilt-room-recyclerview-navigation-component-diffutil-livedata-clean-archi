package com.mazapps.template.model.dto

data class Query(
    val search: List<Search>?,
    val searchinfo: Searchinfo?
)